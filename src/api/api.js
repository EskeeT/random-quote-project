//import fetch from 'cross-fetch';
const header = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'multipart/form-data',
};

export const GET = (url) =>
  fetch(url, {
    method: 'GET',
    cache: 'no-cache',
    header,
  }).then((r) => {
    if (!r.ok) throw new Error(`Request to ${r.url} failed.  ${r.statusText} (${r.status}).`);

    return r.json();
  });
