import { useState, useEffect } from 'react';

import { getQuotes } from '../../api/quotes';
import { Button, Box } from './styled';

export const Quotes = () => {
  const [quote, setQuote] = useState('');
  const [author, setAuthor] = useState('');

  const handleSend = async () => {
    try {
      const res = await getQuotes();
      let data = res.quotes;
      let randomNum = Math.floor(Math.random() * data.length);
      let randomQuote = data[randomNum];

      setQuote(randomQuote.quote);
      setAuthor(randomQuote.author)
    } catch {
      throw new Error('Получены не верные данные.');
    }
  };

  useEffect(() => {
    handleSend()
  }, []);


  return (
    <>
      <Box style={{ height: '60px' }} >"{quote}"</Box>
      <Box>{author}</Box>
      <Box>
        <Button onClick={() => handleSend()}>Generate</Button>
      </Box>
    </>
  )
}
