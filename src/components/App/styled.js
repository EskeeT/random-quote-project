import styled, { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  body {
    -webkit-font-smoothing: antialiased;
    background-color: #e1e3e6;
    margin: 0;
    padding: 0;
    font-family: -apple-system, BlinkMacSystemFont, sans-serif;
}
input, textarea{
  font-family: -apple-system, BlinkMacSystemFont, sans-serif;
  font-size: 16px;
}
`;

export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  width: 100vw;
`;

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 800px;
  height: 100%;
  background-color: white;
`;

export const H1 = styled.h1`
  text-align: center;
  margin-top: 30px;
  color: #2684ff;
`;
