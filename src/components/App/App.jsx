import { GlobalStyle, Wrapper, Container, H1 } from './styled';
import { Quotes } from "../Quotes/Quotes";


export const App = () => {
  return (
    <>
      <GlobalStyle />
      <Wrapper >
        <Container>
          <H1>Random Quote Generator</H1>
          <Quotes />
        </Container>
      </Wrapper>
    </>
  );
}

